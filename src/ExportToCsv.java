
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ExportToCsv {
    public ExportToCsv(String a){
        try {
            
        PrintWriter pw = new PrintWriter(new File("MovieAggregator.csv"));
        pw.write(a);
        pw.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ExportToCsv.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ExportToCsv.class.getName()).log(Level.SEVERE, null, ex);
        }


    }

}
# README #

### What is this repository for? ###

Programming Challenge: Movie Aggregator

Part A:

Create a console-based application that accepts movie details like Name, Run Time, Language, Lead Actor and Genre and then export these details into one of the 2 formats, depending on user’s choice - “plain text” or “PDF”.

Part B:

Make the application extensible so that it is easy to “plug in” a new format and the application automatically has the capability to export to the new format without making any changes to the application code.


Hint: Use Reflection (refer to the code samples in the link) or similar capabilities supported by your language,

Example: Google does not make changes to Chrome each time a new plugin for Chrome is published. The plugin effectively provides new capability without changing any code in Chrome.

Stuff that really matters:


Always code as if the guy who ends up maintaining/extending your code will be a violent psychopath who knows where you live :-)
Check out the SOLID principles and apply them in your code.

### How do I get set up? ###

Java program : execute src/TextDemo.java
Ouptut is generated in the same directory as movie.txt or movie.pdf file.

